import React from "react";
import { connect } from "react-redux";
import { increment, decrement, valueSet, reset } from "./redux/counter";
import "./App.css";
const mapStateToProps = (state) => {
  return {
    count: state.count,
    max: state.isMax,
  };
};
const mapDispatchToProps = () => {
  return {
    increment,
    decrement,
    valueSet,
    reset,
  };
};
class App extends React.Component {
  render() {
    console.log(this.props.max);
    return (
      <div>
        <h1 className="output">{this.props.count}</h1>
        {this.props.max && <div className="max-value">Max value reached</div>}
        <div className="min-max-div">
          <div>
            <p>Min value</p>
            <div className="value-buttons">
              <button value={5} onClick={(event) => this.props.valueSet(event)}>
                5
              </button>
              <button
                value={10}
                onClick={(event) => this.props.valueSet(event)}
              >
                10
              </button>
              <button
                value={15}
                onClick={(event) => this.props.valueSet(event)}
              >
                15
              </button>
            </div>
          </div>
          <div>
            <p>Max value</p>
            <div className="value-buttons">
              <button
                value={20}
                onClick={(event) => this.props.valueSet(event)}
              >
                20
              </button>
              <button
                value={50}
                onClick={(event) => this.props.valueSet(event)}
              >
                50
              </button>
              <button
                value={100}
                onClick={(event) => this.props.valueSet(event)}
              >
                100
              </button>
            </div>
          </div>
        </div>

        <div className="inc-dec-buttons">
          <button onClick={() => this.props.increment()}>Increment</button>
          <button onClick={() => this.props.decrement()}>Decrement</button>
          <button onClick={() => this.props.reset()}>Reset</button>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps())(App);
