let INCREMENT = "INCREMENT";
let DECREMENT = "DECREMENT";
let SETTING = "SETTING";
let RESET = "RESET";
export let increment = () => {
  return {
    type: INCREMENT,
  };
};
export let decrement = () => {
  return {
    type: DECREMENT,
  };
};
let targetValue = null;
export let valueSet = (event) => {
  targetValue = event.target.value;
  return {
    type: SETTING,
  };
};
export let reset = () => {
  return {
    type: RESET,
  };
};
let initialState = {
  count: 0,
  add: 1,
  max: 200,
  isClicked: false,
  isMax: false,
};
const counter = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      let value = Number(state.count) + Number(state.add);
      if (value <= Number(state.max)) {
        return {
          ...state,
          count: value,
        };
      } else {
        return {
          ...state,
          count: state.max,
          isMax: true,
          isClicked: false,
        };
      }

    case DECREMENT:
      let val = state.count - Number(state.add);
      if (val > state.max * -1) {
        return {
          ...state,
          count: val,
          isMax: false,
        };
      } else {
        return {
          count: state.max * -1,
          isMax: true,
        };
      }

    case SETTING:
      if (!state.isClicked) {
        return {
          ...state,
          add: targetValue,
          isClicked: true,
        };
      } else {
        return {
          ...state,
          max: targetValue,
        };
      }
    case RESET:
      return {
        ...state,
        count: 0,
        add: 1,
        endValue: 200,
        isClicked: false,
        isMax: false,
      };

    default:
      return state;
  }
};
export default counter;
